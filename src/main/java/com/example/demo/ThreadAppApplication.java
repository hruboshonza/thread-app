package com.example.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThreadAppApplication {

	public static void main(String[] args) {
		new Thread(new MyThread(5, 10)).start();
		new Thread(new MyThread(0, 4)).start();
	}
}