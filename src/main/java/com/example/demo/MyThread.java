package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyThread extends Thread {
    public static final Object lock = new Object();
    private final Logger l = LoggerFactory.getLogger(MyThread.class);

    public int notifyAfter;
    public int notifyStop;

    public MyThread(int notifyAfter, int notifyStop) {
        this.notifyAfter = notifyAfter;
        this.notifyStop = notifyStop;
    }

    @Override
    public synchronized void run() {
        synchronized (lock) {
            try {
                for (int i = 0; i <= 10; i++) {
                    l.info(String.valueOf(i));
                    if (notifyAfter <= i && i < notifyStop+1) {
                        lock.notify();
                        lock.wait();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
